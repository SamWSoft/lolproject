import React from "react";
import { Toolbar, Typography, AppBar, Button } from "@material-ui/core";
import SummonerSearch from "./forms/search/SummonerSearch";

class Nav extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <AppBar position="static">
        <Toolbar>
          <Typography
            style={{ flexGrow: "1", marginLeft: "10px" }}
            variant="h5"
            color="inherit"
          >
            Dumpster Bros
          </Typography>

          <SummonerSearch />

          <Button style={{ float: "right" }} color="inherit">
            Login
          </Button>
        </Toolbar>
      </AppBar>
    );
  }
}

export default Nav;
