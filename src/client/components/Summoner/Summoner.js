import React from "react";
import { Toolbar, Typography, Button, AppBar } from "@material-ui/core";
import "./Summoner.css";

class Summoner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      summoner: {},
      isLoading: false
    };
  }

  getSummoner = () => {
    this.setState({ isLoading: true });

    fetch("/api/summoners/" + this.props.summonerName)
      .then(res => res.json())
      .then(res => this.setState({ summoner: res, isLoading: false }));
  };

  componentDidMount() {
    //  this.getSummoner();
  }

  componentWillReceiveProps(newProps) {
    console.log("loaded");
  }

  render() {
    const { summonerName } = this.props;
    const { summoner } = this.state;
    return (
      <div className="summoner">
        <Typography>Summoner: {summonerName}</Typography>
        <Typography>Level: {summoner && summoner.summonerLevel}</Typography>
      </div>
    );
  }
}

export default Summoner;
