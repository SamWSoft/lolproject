import axios from "axios";

export function getSummonerByName(name) {
  return axios.get("/summoners/name/" + name).then(function(response) {
    return response.data;
  });
}
