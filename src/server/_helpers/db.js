const config = require("../config.json");
const mongoose = require("mongoose");

mongoose.connect(process.env.MONGODB_URI || config.dbConnectionString, {
  useCreateIndex: true,
  useNewUrlParser: true
});
mongoose.Promise = global.Promise;

module.exports = {
  User: require("../models/users/user.model"),
  Summoner: require("../models/summoners/summoner.model")
};
