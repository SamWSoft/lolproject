const db = require("../../_helpers/db");
const Summoner = db.Summoner;

module.exports = {
  // getAll,
  getById,
  getByName,
  create,
  update
};

// async function getAll() {
//   return await Summoner.find().select("-hash");
// }

async function getById(id) {
  return await Summoner.findById(id).select("-hash");
}

async function getByName(name) {
  return await Summoner.findOne({ name: name }).select("-hash");
}

async function create(summonerParam) {
  
  const smner = new Summoner(summonerParam);
  await smner.save();
}

async function update(id, summonerParam) {
  const smner = await Summoner.findById(id);

  // validate
  if (!smner) throw "Summoner not found";
  Object.assign(smner, summonerParam);

  await smner.save();
}

// async function _delete(id) {
//   await Summoner.findByIdAndRemove(id);
// }
