const express = require("express");
const router = express.Router();
const summonerService = require("./summoner.service");
const config = require("../../config.json");
const request = require("request");
// routes
router.post("/create", create);
//router.get("/", getAll);
router.get("/:id", getById);
//router.get("/:name", getByName);
router.put("/:id", update);
//router.delete("/:id", _delete);

/* GET summoner by name. */
router.get("/name/:name", async (req, res,next) => {
  //check for db entry first, if not add and return
  const summoner = await summonerService
    .getByName(req.params.name)
    .then(summoner => summoner)
    .catch(err => next(err));

  if (!summoner) {
    var newSummoner = await summonerByName(req.params.name);
    
    const result  = await summonerService
    .create(JSON.parse(newSummoner))
    .then((summonerResponse) => summonerResponse.json({}))
    .catch(err => err);

    console.log("Summorerer Created");

    res.send(result);
  } else {
    console.log("Summoner Already Exists");
    res.send(newSummoner);
  }
});

module.exports = router;

function getById(req, res, next) {
  summonerService
    .getById(req.params.id)
    .then(summoner => (summoner ? res.json(summoner) : res.sendStatus(404)))
    .catch(err => next(err));
}

function getByName(req, res, next) {
  summonerService
    .getByName(req.params.name)
    .then(summoner => (summoner ? res.json(summoner) : res.sendStatus(404)))
    .catch(err => next(err));
}

function create(req, res, next) {
  summonerService
    .create(req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function update(req, res, next) {
  summonerService
    .update(req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

async function summonerByName(name) {
  return new Promise(resolve => {
    request(
      `${config.riotApi}/lol/summoner/v4/summoners/by-name/${name}?api_key=${
        config.apiKey
      }`,
      (error, response, body) => {
        if (!error && response.statusCode === 200) {
          const result = JSON.stringify(JSON.parse(body));
          return resolve(result, false);
        }
        return resolve(null, error);
      }
    );
  });
}
