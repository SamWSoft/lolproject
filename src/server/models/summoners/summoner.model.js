const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  puuid: { type: String, unique: true, required: true },
  name: { type: String, required: true },
  id: { type: String, required: true },
  accountId: { type: String, required: true },
  revisionDate: { type: Date, default: Date.now },
  summonerLevel: { type: Number, required: true }
});

schema.set("toJSON", { virtuals: true });

module.exports = mongoose.model("Summoner", schema);
