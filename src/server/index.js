require("rootpath")();
const express = require("express");
const app = express();
const cors = require("cors");
const request = require("request");
const bodyParser = require("body-parser");
const jwt = require("./_helpers/jwt");
const errorHandler = require("./_helpers/error-handler");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
//app.use(jwt());

app.use("/users", require("./models/users/users.controller"));
app.use("/summoners", require("./models/summoners/summoners.controller"));

app.use(express.static("dist"));
app.use(errorHandler);

app.listen(process.env.PORT || 8080, () =>
  console.log(
    `Dumpster Bros server started - http://localhost:${process.env.PORT ||
      8080}`
  )
);
